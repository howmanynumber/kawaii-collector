# kawaii-collector makefile
CONFDIR=/etc/kawaii-collector
BINDIR=/usr/local/bin

all: dep build
build:
	go build -v -o kawaii-collector src/main.go
clean:
	go clean
	rm -f kawaii-collector
install:
	sudo mkdir -p $(CONFDIR)
	if [ ! -e $(CONFDIR)/kawaii-collector.conf ]; then\
		pwd;\
		sudo cp etc/kawaii-collector/kawaii-collector.conf $(CONFDIR);\
		sudo cp kawaii-collector $(BINDIR);\
	fi
uninstall:
	sudo rm -f $(BINDIR)/kawaii-collector
	sudo rm -rf $(CONFDIR)
dep:
	go get -v github.com/nlopes/slack
	go get -v github.com/ChimeraCoder/anaconda
	go get -v bitbucket.org/howmanynumber/kawaii-collector/src/config
	go get -v bitbucket.org/howmanynumber/kawaii-collector/src/channel
	go get -v bitbucket.org/howmanynumber/kawaii-collector/src/util
	go get -v bitbucket.org/howmanynumber/kawaii-collector/src/twitter
	go get -v bitbucket.org/howmanynumber/kawaii-collector/src/slack
