#FROM ubuntu
FROM resin/rpi-raspbian

MAINTAINER hownamynumber <@howmanynumber1>

COPY kawaii-collector /usr/bin

RUN mkdir /etc/kawaii-collector && \
    apt-get update && \
    apt-get install -y cifs-utils

CMD ["/usr/bin/kawaii-collector"]
