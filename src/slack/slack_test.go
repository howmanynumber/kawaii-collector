package slack

import (
    "testing"
    "bitbucket.org/howmanynumber/kawaii-collector/src/config"
)

func TestIsApiAvailable(t *testing.T) {
    config.Slconf = map[string]string{
        "bot_token": "testtoken",
    }
    BotAuth()
    err := IsApiAvailable()
    if err == nil {
        t.Errorf("Invalid API returns active result...")
    }
}

func TestBotAuth(t *testing.T) {
    config.Slconf = map[string]string{
        "bot_token": "testtoken",
    }
    BotAuth()
    err := IsApiAvailable()
    if err == nil {
        t.Errorf("Invalid API returns active result...")
    }
}
