package slack

import (
    sl "github.com/nlopes/slack"
    "bitbucket.org/howmanynumber/kawaii-collector/src/config"
    "bitbucket.org/howmanynumber/kawaii-collector/src/channel"
)

var (
    api *sl.Client
    rtm *sl.RTM
)

func IsApiAvailable() error {
    _, err := api.GetUserInfo("U023BECGF")
    if err != nil {
	    return err
    }
    return nil
}

func BotAuth() {
    api = sl.New(config.Slconf["bot_token"])
}


func handleSlackChannel() {
    for {
        select {
        case str := <-channel.Slchannel:
            rtm.SendMessage(rtm.NewOutgoingMessage(str, config.Slconf["channel"]))
        }
    }
}


func Run() {
    rtm = api.NewRTM()
    go rtm.ManageConnection()
    go handleSlackChannel()

    for {
        select {
        case msg := <-rtm.IncomingEvents:
            switch ev := msg.Data.(type) {
            case *sl.MessageEvent:
                if ev.Text == "!channel" {
                    // for check channel id
                    rtm.SendMessage(rtm.NewOutgoingMessage(ev.Channel, ev.Channel))
                } else {
                    // for use twitter
                    channel.Twchannel <- ev.Text
                }
            case *sl.InvalidAuthEvent:
                println("Invalid credentials")
                return
            }
        }
    }
}
