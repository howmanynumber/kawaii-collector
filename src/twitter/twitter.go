package twitter

import (
    "time"
    "net/url"
    "errors"
    "strings"
    "strconv"
    "github.com/ChimeraCoder/anaconda"
    "bitbucket.org/howmanynumber/kawaii-collector/src/config"
    "bitbucket.org/howmanynumber/kawaii-collector/src/channel"
    "bitbucket.org/howmanynumber/kawaii-collector/src/util"
)

var (
    userId int64
    listId int64
    api *anaconda.TwitterApi
    stream *anaconda.Stream
    sinceId int64 = 0
)

func isApiAvailable() error {
    v := url.Values{}
    user, err := api.GetSelf(v)
    if err != nil {
        return errors.New("API is unavailable...")
    }
    userId = user.Id
    return nil
}

func UserAuth() {
    anaconda.SetConsumerKey(config.Twconf["consumer_key"])
    anaconda.SetConsumerSecret(config.Twconf["consumer_secret"])
    api = anaconda.NewTwitterApi(config.Twconf["access_token"], config.Twconf["access_token_secret"])
    err := isApiAvailable()
    if err != nil {
        println(err.Error())
    }
}

func getListId() error {
    v := url.Values{}
    lists, err := api.GetListsOwnedBy(userId, v)
    if err != nil {
        return err
    }
    for _, list := range lists {
        if list.Name == config.Twconf["list_name"] {
            listId = list.Id
            return nil
        }
    }
    return errors.New("Specified list was not found...")
}

// Check Timeline
func Run() {
    err := getListId()
    if err != nil {
        println(err.Error())
        return
    }
    go handleTwitterChannel()

    for {
        getListPictures()
        time.Sleep(3 * time.Second)
    }
}


// https://developer.twitter.com/en/docs/accounts-and-users/create-manage-lists/api-reference/get-lists-statuses
func getListPictures() {
    v := url.Values{}
    if sinceId != 0 {
        v.Set("since_id", strconv.FormatInt(sinceId, 10))
    } else {
        v.Set("count", "10")
    }

    tweets, err := api.GetListTweets(listId, true, v)
    if err != nil {
        println("Failed to get timeline")
        channel.Slchannel <- "Failed to get timeline"
        apierr := isApiAvailable()
        if apierr != nil {
            UserAuth()
        }
        return
    }

    for _, t := range tweets {
        if t.Id > sinceId {
            sinceId = t.Id
        }
        if len(t.Entities.Media) != 0 {
            println(t.User.Name + " tweeted with picture!!!")
            for _, media := range t.Entities.Media {
                err = GetPictureFromTweet(t, media)
                if err != nil {
                    channel.Slchannel <- "Failed to save picture!"
                }
            }
        }
    }
}

func createTweetAnnounceMsg(user string, content string) string {
    msg := user + " tweeted!!"
    msg = msg + "\n```\n" + content + "\n```"
    return msg
}

// Handle commands from slack
func handleTwitterChannel() {
    for {
        select {
        case str := <-channel.Twchannel:
            command := strings.Fields(str)
            if len(command) > 1 {
                switch command[0] {
                case "!follow":
                    println("Follow " + command[1])
                    channel.Slchannel <- watchAccount(command[1])
                }
            }
        }
    }
}

func watchAccount(username string) string {
    followResp := followByAccountName(username)
    listResp := addFollowedUserToList(username)
    return followResp + "\n" + listResp
}

// Follow specified account by username
func followByAccountName(username string) string {
    //v := url.Values{}
    user, err := api.FollowUser(username)
    if err != nil {
        println(err.Error())
        return "Failed to follow @" + username
    }
    msg := "Followed " + user.Name + "(@" + user.ScreenName + ")"
    return msg
}

func addFollowedUserToList(username string) string {
    v := url.Values{}
    _, err := api.AddUserToList(username, listId, v)
    if err != nil {
        println(err.Error())
        return "Failed to add @" + username + " to list " + config.Twconf["list_name"]
    }
    return "Added @" + username + " to list " + config.Twconf["list_name"]
}

func GetPictureFromTweet(t anaconda.Tweet, media anaconda.EntityMedia) error {
    if media.Type == "photo" {
        ext := util.ParseExtract(media.Media_url_https)
        path, err := util.CreateFilePath(t.User.ScreenName, t.CreatedAt, ext)
        if err != nil {
            return err
        } else {
            println("URL:  " + media.Media_url_https)
            println("Path: " + path)
            picerr := util.GetPicture(media.Media_url_https, path)
            if picerr != nil {
                return picerr
            }
        }
    }
    return nil
}
