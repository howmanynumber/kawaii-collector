package twitter

import (
    "testing"
    "bitbucket.org/howmanynumber/kawaii-collector/src/config"
)

func TestIsApiAvailable(t *testing.T) {
    config.Twconf = map[string]string{
        "consumer_key": "test_consumer_key",
        "consumer_secret": "test_consumer_secret",
        "access_token": "test_access_token",
        "access_token_secret": "test_access_token_secret",
    }
    UserAuth()
    err := isApiAvailable()
    if err == nil {
        t.Errorf("dummy api is available")
    }
    err = config.Init()
    if err != nil {
        t.Errorf("Failed to load conf data")
    }
    UserAuth()
    err = isApiAvailable()
    if err != nil {
        t.Errorf("Failed to login using true data")
    }
}

func TestUserAuth(t *testing.T) {
    // test dummy credential
    config.Twconf = map[string]string{
        "consumer_key": "test_consumer_key",
        "consumer_secret": "test_consumer_secret",
        "access_token": "test_access_token",
        "access_token_secret": "test_access_token_secret",
    }
    UserAuth()
    tof, err := api.VerifyCredentials()
    if tof {
        t.Errorf("Part1: Succeeded to login using dummy credential??")
    }
    if err == nil {
        t.Errorf("Part2: Succeeded to login using dummy credential??")
    }
    // test true data
    err = config.Init()
    if err != nil {
        t.Errorf("Failed to load conf data")
    }
    UserAuth()
    tof, err = api.VerifyCredentials()
    if err != nil {
        t.Errorf("Part1: Failed to login using true data")
    }
    if !tof {
        t.Errorf("Part2: Failed to login using true data")
    }
}
