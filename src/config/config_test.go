package config

import (
    "testing"
)

func TestInit(t *testing.T) {
    configFile = "../../etc/kawaii-collector/kawaii-collector.conf"
    err := Init()
    if err != nil {
        t.Errorf("Failed to read dummy file")
    }
}

func TestLoadDefaultConf(t *testing.T) {
    configFile = "dummifilename.conf"
    err := loadDefaultConf()
    if err == nil {
        t.Errorf("Dummy file exists")
    }

    configFile = "../../etc/kawaii-collector/kawaii-collector.conf"
    err = loadDefaultConf()
    if err != nil {
        t.Errorf(err.Error())
    }

    if Defconf["image_base_path"] != "/hoge/hogehoge" {
        t.Errorf("bot_token has wrong value: " + Defconf["image_base_path"])
    }

}


func TestLoadTwitterConf(t *testing.T) {
    configFile = "dummifilename.conf"
    err := loadTwitterConf()
    if err == nil {
        t.Errorf("Dummy file exists")
    }

    configFile = "../../etc/kawaii-collector/kawaii-collector.conf"
    err = loadTwitterConf()
    if err != nil {
        t.Errorf(err.Error())
    }

    if Twconf["consumer_key"] != "test_consumer_key" {
        t.Errorf("consumer_key has wrong value: " + Twconf["consumer_key"])
    }
    if Twconf["consumer_secret"] != "test_consumer_secret" {
        t.Errorf("consumer_secret has wrong value: " + Twconf["consumer_secret"])
    }
    if Twconf["access_token"] != "test_access_token" {
        t.Errorf("access_token has wrong value: " + Twconf["access_token"])
    }
    if Twconf["access_token_secret"] != "test_access_token_secret" {
        t.Errorf("access_token_secret has wrong value: " + Twconf["access_token_secret"])
    }
    if Twconf["list_name"] != "test_list_name" {
        t.Errorf("list_name has wrong value: " + Twconf["list_name"])
    }
}

func TestLoadSlackConf(t *testing.T) {
    configFile = "dummifilename.conf"
    err := loadSlackConf()
    if err == nil {
        t.Errorf("Dummy file exists")
    }

    configFile = "../../etc/kawaii-collector/kawaii-collector.conf"
    err = loadSlackConf()
    if err != nil {
        t.Errorf(err.Error())
    }

    if Slconf["bot_token"] != "testtoken" {
        t.Errorf("bot_token has wrong value: " + Slconf["bot_token"])
    }
    if Slconf["channel"] != "testchannel" {
        t.Errorf("channel has wrong value: " + Slconf["channel"])
    }
}
