package config

import (
    "github.com/go-ini/ini"
)

var (
    configFile = "/etc/kawaii-collector/kawaii-collector.conf"
    Defconf map[string]string
    Twconf map[string]string
    Slconf map[string]string
)

func Init() error {
    deferr := loadDefaultConf()
    if deferr != nil {
        return deferr
    }
    twerr := loadTwitterConf()
    if twerr != nil {
        return twerr
    }
    slerr := loadSlackConf()
    if slerr != nil {
        return slerr
    }
    return nil
}

func loadDefaultConf() error {
    Defconf = map[string]string{}
    cfg, err := ini.InsensitiveLoad(configFile)
    if err != nil {
        return err
    }
    section, err := cfg.GetSection("default")
    if err != nil {
        return err
    }

    image_base_path, err := section.GetKey("image_base_path")
    if err != nil {
        return err
    }

    Defconf["image_base_path"] = image_base_path.MustString("none")

    return nil
}

func loadTwitterConf() error {
    Twconf = map[string]string{}
    cfg, err := ini.InsensitiveLoad(configFile)
    if err != nil {
        return err
    }
    section, err := cfg.GetSection("twitter")
    if err != nil {
        return err
    }

    consumer_key, err := section.GetKey("consumer_key")
    if err != nil {
        return err
    }
    consumer_secret, err := section.GetKey("consumer_secret")
    if err != nil {
        return err
    }
    access_token, err := section.GetKey("access_token")
    if err != nil {
        return err
    }
    access_token_secret, err := section.GetKey("access_token_secret")
    if err != nil {
        return err
    }
    list_name, err := section.GetKey("list_name")

    Twconf["consumer_key"] = consumer_key.MustString("none")
    Twconf["consumer_secret"] = consumer_secret.MustString("none")
    Twconf["access_token"] = access_token.MustString("none")
    Twconf["access_token_secret"] = access_token_secret.MustString("none")
    Twconf["list_name"] = list_name.MustString("none")

    return nil
}

func loadSlackConf() error {
    Slconf = map[string]string{}
    cfg, err := ini.InsensitiveLoad(configFile)
    if err != nil {
        return err
    }
    section, err := cfg.GetSection("slack")
    if err != nil {
        return err
    }

    bot_token, err := section.GetKey("bot_token")
    if err != nil {
        return err
    }
    channel, err := section.GetKey("channel")
    if err != nil {
        return err
    }

    Slconf["bot_token"] = bot_token.MustString("none")
    Slconf["channel"] = channel.MustString("none")

    return nil
}
