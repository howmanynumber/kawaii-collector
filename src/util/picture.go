package util

import (
    "io/ioutil"
    "net/http"
    "strconv"
    "strings"
    "os"
    "bitbucket.org/howmanynumber/kawaii-collector/src/config"
)

var (
    month = map[string]string{
        "Jan": "01",
        "Feb": "02",
        "Mar": "03",
        "Apr": "04",
        "May": "05",
        "Jun": "06",
        "Jul": "07",
        "Aug": "08",
        "Sep": "09",
        "Oct": "10",
        "Nov": "11",
        "Dec": "12",
    }
)


func GetPicture(url string, path string) error {
    response, err := http.Get(url)
    if err != nil {
        return err
    }

    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        return err
    }

    file, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0666)
    if err != nil {
        return err
    }

    file.Write(body)
    file.Close()

    return nil
}

func CreateFilePath(accountName string, date string, extract string) (string, error) {
    path := config.Defconf["image_base_path"] + "/" + accountName
    err := pathExistsOrCreate(path)
    if err != nil {
        return "", err
    }
    // Wed, 03 Oct 2012 00:00:00 +0000
    splittedDate := strings.Fields(date)
    splittedDate[3] = strings.Replace(splittedDate[3], ":", "", -1)
    name := splittedDate[5] + month[splittedDate[1]] + splittedDate[2] + "_" + splittedDate[3] + "_"
    tmpPath := path + "/" + name

    index := 0
    fullPath := tmpPath + strconv.Itoa(index) + "." + extract
     _, exists := os.Stat(fullPath)
    for exists == nil {
        index = index + 1
        fullPath = tmpPath + strconv.Itoa(index) + "." + extract
        _, exists = os.Stat(fullPath)
    }
    return fullPath, nil
}

func ParseExtract(picurl string) string {
    fields := strings.Split(picurl, ".")
    fields2 := strings.Split(fields[len(fields) - 1], ":")
    return fields2[0]
}

func pathExistsOrCreate(path string) error {
    _, err := os.Stat(path)
    if err != nil {
        direrr := os.MkdirAll(path, 0777)
        if direrr != nil {
            return direrr
        }
    }
    return nil
}
