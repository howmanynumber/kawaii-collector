package util

import (
    "testing"
)

func TestParseExtract(t *testing.T) {
    sampledata := "https://pbs.twimg.com/media/hogehoge.jpg:large"
    result := ParseExtract(sampledata)
    println("Result: " + result)
    if result != "jpg" {
        t.Errorf("Expected: jpg")
    }
}

func TestPathExistsOrCreate(t *testing.T) {
    path := "../../etc/kawaii-collector/kawaii-collector.conf"
    err := pathExistsOrCreate(path)
    if err != nil {
        t.Errorf("Failed to check or create the path...")
    }
}
