package main

import(
    "bitbucket.org/howmanynumber/kawaii-collector/src/config"
    "bitbucket.org/howmanynumber/kawaii-collector/src/twitter"
    "bitbucket.org/howmanynumber/kawaii-collector/src/slack"
    "bitbucket.org/howmanynumber/kawaii-collector/src/channel"
)

func main(){
    err := config.Init()
    if err != nil {
        println("Failed to read config file...")
        println("Exit the application.")
        return
    }

    twitter.UserAuth()
    slack.BotAuth()

    channel.Init()
    go twitter.Run()
    slack.Run()
}
