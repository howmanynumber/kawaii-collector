package channel

var (
    Twchannel chan string
    Slchannel chan string
)

func Init() {
    Twchannel = make(chan string, 50)
    Slchannel = make(chan string, 50)
}
