package channel

import (
    "testing"
)

func TestInit(t *testing.T) {
    Init()
    Twchannel <- "twitter channel test"
    str := <- Twchannel
    if str != "twitter channel test" {
        t.Errorf("Failed init Twchannel")
    }
    Slchannel <- "slack channel test"
    str = <- Slchannel
    if str != "slack channel test" {
        t.Errorf("Failed init Swchannel")
    }
}
